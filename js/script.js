//##########################################
//DYNATIME - DYNAMIC TIMELINE PLUGIN
//FEATURES:
//- BUILD IT WITH JAVASCRIPT ARRAY
//- ANIMATED
//##########################################

var dynatime = function () {
  var defaults = {
    'domNode': '',
    'infoView': 'div#info',
    'hideTextWhenLoad': false,
    'anim': 'fadeIn',
    'template': dot_templates,
    'timleine': [], // REQUIRED: the array that holds all the timeline phases and it's details
    'controls': event_controls, // REQUIRED: the object that contains the template of different controls
    'typeOfConnections': [], // REQUIRED: the array that holds all the type of possible connections
    'speedOfAnimation': 200, // default is 200

    'canEdit': true,
    'canAddNewEvent': true,
    'canDeleteEvent': true,
  };

  return {
    selectedModel: undefined,
    selectedIndex: undefined,
    currentAction: undefined,
    actionsList: undefined,
    optionsList: undefined,
    formWrapper: undefined,
    defaultSelectionNode: undefined,
    newInfoHolder: {},

    init: function () {
      // setup all the references
      this.infoView = $(defaults.infoView);
      if (defaults.timeline.length > 0) this.printPhases();
      this.showHideInfoLayer();
    },

    initWithSettings: function(settings) {
      if (settings && typeof settings == 'object') {
        defaults = jQuery.extend(defaults, settings);
      }
      // init methods for custom behaviors
      this.init();
    },

    /*---------------------------------------------------------------------
    end of default initers
    ---------------------------------------------------------------------*/

    printPhases: function () {
      var phase, tmpl;
      for (index in defaults.timeline) {
        phase = defaults.timeline[index];
        if (phase.type) {
          tmpl = (defaults.template[phase.type]) || '';
          // replaces the title to actual parsed title from array
          tmpl = tmpl.replace('Sample', (phase.title || '&nbsp;'));
          // replaces the caption to actual parsed caption from array
          tmpl = tmpl.replace('Caption', (phase.caption || '&nbsp;'));
          // replaces the href to actual parsed link from array
          tmpl = tmpl.replace('#', '#' + (phase.title || '&nbsp;'));
        }
        $(tmpl).attr({
          'data-info':phase.info,
          'data-id': index
        }).hide().appendTo(defaults.domNode);
        if (phase)
          this.setAnimationToNode($($('div.node')[index]), index, defaults.anim);
      }
      tmpl = defaults.template['Add New'];
      $(tmpl).hide().appendTo(defaults.domNode);
      this.setAnimationToNode(
        $($('div.node')[(defaults.timeline.length)]),
        (defaults.timeline.length), defaults.anim
      );

      $('<div class="flush"></div>').appendTo(defaults.domNode);
      if (defaults.hideTextWhenLoad) {
        // we hide the texts so they don't show up
        this.hideAllText();
      }
      this.behavior();
    },

    printTypeOfConnections: function (parentNode) {
      var con, tmpl;
      if (!parentNode && !$(parentNode)) return;
      for (index in defaults.typeOfConnections) {
        con = defaults.typeOfConnections[index];
        tmpl = '<li>'+con+'</li>';
        $(tmpl).appendTo(parentNode);
      }
    },

    printTypeOfControls: function (parentNode) {
      if (!parentNode && !$(parentNode)) return;
      console.log(this.currentAction);
      if (defaults.canEdit && this.currentAction != 'new') {
        $('<li>'+defaults.controls['edit']+'</li>').appendTo(parentNode);
      }
      if (defaults.canAddNewEvent) {
        $('<li>'+defaults.controls['new']+'</li>').appendTo(parentNode).
        find('a').addClass(((this.currentAction == 'new') ? 'selected' : ''));
      }
      if (defaults.canDeleteEvent && this.currentAction != 'new') {
        $('<li>'+defaults.controls['delete']+'</li>').appendTo(parentNode);
      }
    },

    printInfo: function (info) {
      $(this.infoView).hide().html(info).fadeIn();
    },

    /*---------------------------------------------------------------------
    end of data printers
    ---------------------------------------------------------------------*/
    
    setAnimationToNode: function (node, index, type) {
      switch(type) {
        case 'fadeIn':
          $(node).delay(defaults.speedOfAnimation * index).fadeIn();
          break;
        case 'slideIn':
          $(node).delay(defaults.speedOfAnimation * index).fadeIn();
          $(node).find('div.dot-wrapper').css('width',0)
            .delay(defaults.speedOfAnimation * index).animate({ width: 99 }, defaults.speedOfAnimation);
          break;
      }
    },

    onShowText: function (node) {
      $(node).parent().find('h2').animate({ opacity: 1 }, defaults.speedOfAnimation);
      $(node).parent().find('p').animate({ opacity: 1 }, defaults.speedOfAnimation);
    },

    hideAllText: function () {
      $('div.node').each(function(index, node) {
        // we check if a phase is selected
        // if selected, we don't hide it
        if (!$(node).hasClass('selected') && !$(node).hasClass('add-new')) {
          $(node).find('h2').stop(true,false).animate({ opacity: 0 }, defaults.speedOfAnimation);
          $(node).find('p').stop(true,false).animate({ opacity: 0 }, defaults.speedOfAnimation);
        }
      });
    },

    /*---------------------------------------------------------------------
    end of Timeline management & handlers
    ---------------------------------------------------------------------*/

    showHideInfoLayer: function (show) {
      if (show) $(this.infoView).fadeIn(); else $(this.infoView).hide();
    },

    changeFormTitle: function (msg) {
      if (msg) $(this.infoView).find('h2.form-title').html(msg);
    },

    /*---------------------------------------------------------------------
    end of Info panel handlers
    ---------------------------------------------------------------------*/

    resetSelection: function () {
      $('div.node').removeClass('selected');
      if (defaults.hideTextWhenLoad) this.hideAllText();
      this.currentAction = '';
    },

    refreshTimeline: function () {
      this.newInfoHolder = {};
      this.selectedModel = undefined;
      this.selectedIndex = undefined;
      $(defaults.domNode).empty();
      this.printPhases();
      this.showHideInfoLayer(false);
    },

    updateRecord: function () {
      // if other fields are not edited, it will not be inserted
      // so we need to perform another round of recording
      this.newInfoHolder[$(this.amountField).attr('id')] = $(this.amountField).val();
      this.newInfoHolder[$(this.typeField).attr('id')] = $(this.typeField).val();
      this.newInfoHolder[$(this.captionField).attr('id')] = $(this.captionField).val();
      this.newInfoHolder[$(this.infoField).attr('id')] = $(this.infoField).val();

      this.selectedModel = this.newInfoHolder;
      if (this.currentAction == 'new') {
        // if it's a new record, we set the index to total length
        this.selectedIndex = defaults.timeline.length;        
      }
      defaults.timeline[this.selectedIndex] = this.selectedModel;
      
      this.refreshTimeline();
    },

    /*---------------------------------------------------------------------
    end of record refresher handlers
    ---------------------------------------------------------------------*/

    showEditFormWithModel: function (model) {
      var _this = this;
      var tmpl = $('#tmpl-edit-form').html();      
      $(this.infoView).append(tmpl);      
      var node = $(this.infoView).find('form#editForm');

      this.actionsList = $(this.infoView).find('ul.action-list');
      this.amountField = $(node).find('input#title');
      this.typeField = $(node).find('input#type');
      this.captionField = $(node).find('textarea#caption');
      this.infoField = $(node).find('textarea#info');

      this.formWrapper = $(node).find('div#typeSelection');
      this.optionsList = $(this.formWrapper).find('ul#optionsList');
      this.defaultSelectionNode = $(this.formWrapper).find('span.default-selection');

      // populate the form with selected model data
      this.fillFormWithData(model || '');

      // populate the type of controls
      this.printTypeOfControls(this.actionsList);

      // populate the type of connections
      this.printTypeOfConnections(this.optionsList);

      $(this.defaultSelectionNode).on('click', function (e) {
        _this.showOptionsList($(_this.optionsList).hasClass('hidden') || false);
      });

      $(this.infoView).find('button#saveBtn').on('click', function (e) {
        // save all the new model info into the data
        _this.updateRecord();
        return false;
      });

      $(this.infoView).find('input').on('keyup', function (e) {
        _this.newInfoHolder[$(e.target).attr('id')] = $(e.target).val();        
      });
      $(this.infoView).find('textarea').on('keyup', function (e) {
        _this.newInfoHolder[$(e.target).attr('id')] = $(e.target).val();
      });

      $(this.actionsList).find('li').on('click', function (e) {
        if ($(e.target).attr('id') == 'deleteAction') {
          _this.onDeleteExistingEntry($(e.target));          
        } else if ($(e.target).attr('id') == 'addAction') {
          _this.onAddingNewEntry();
        } else {
          _this.onEditCurrentEntry();
        }
        _this.onActionSelected($(e.target));
      });
    },

    fillFormWithData: function (model) {
      // replacing the form title
      if (this.currentAction == 'new') {
        this.changeFormTitle('Adding New Event '+ (parseInt(defaults.timeline.length) + 1));
      } else {
        this.changeFormTitle('Editing Event '+ (parseInt(this.selectedIndex) + 1));
      }      

      if (model && typeof model == 'object') {
        $(this.amountField).val(model.title);
        $(this.captionField).val(model.caption);
        $(this.infoField).val(model.info);      

        // change the value of the default-selection to selected model first
        this.defaultSelectionNode.html(model.type);
        $(this.typeField).val(model.type);
      } else {
        $(this.amountField).val(this.selectedModel.title);
        $(this.captionField).val(this.selectedModel.caption);
        $(this.infoField).val(this.selectedModel.info);      

        // change the value of the default-selection to selected model first
        this.defaultSelectionNode.html(this.selectedModel.type);
        $(this.typeField).val(this.selectedModel.type);
      }      
    },

    /*---------------------------------------------------------------------
    end of form management & events
    ---------------------------------------------------------------------*/

    onOptionSelected: function (newSelection) {
      $(this.defaultSelectionNode).html(newSelection);
      $(this.typeField).val(newSelection);
      this.newInfoHolder[$(this.typeField).attr('id')] = $(this.typeField).val();
    },

    showOptionsList: function (show) {
      if (show) {
        $(this.optionsList).removeClass('hidden');
        this.listenToOptionsListChange(this.optionsList);
      } else {
        $(this.optionsList).addClass('hidden');
        this.disconnectOptionsListListener(this.optionsList);
      }
    },

    listenToOptionsListChange: function (node) {
      var _this = this;

      $(node).find('li').on('click', function (e) {
        _this.showOptionsList(false);
        _this.onOptionSelected($(e.target).html());
      });
    },

    disconnectOptionsListListener: function (node) {
      $(node).find('li').off();
    },

    /*---------------------------------------------------------------------
    end of option selection handlers
    ---------------------------------------------------------------------*/
    
    onAddingNewEntry: function (node) {
      this.currentAction = 'new';
      // set the selectedIndex to the total length
      this.changeFormTitle('Editing Event '+ (parseInt(defaults.timeline.length) + 1));
      $(this.amountField).val('');
      $(this.captionField).val('');
      $(this.infoField).val('');
      this.defaultSelectionNode.html('Normal Connection');
      $(this.typeField).val('Normal Connection');
    },

    /*---------------------------------------------------------------------
    end of adding entry handlers
    ---------------------------------------------------------------------*/

    onEditCurrentEntry: function () {
      this.currentAction = 'edit';
      this.fillFormWithData();
    },

    /*---------------------------------------------------------------------
    end of editing entry handlers
    ---------------------------------------------------------------------*/

    onDeleteExistingEntry: function (node) {
      this.currentAction = 'delete';
      var _this = this;
      this.showModalOverlay('body');
      var modalWrapper = $('body').find('div.overlay-wrapper');
      $(modalWrapper).find('h2').html('Deleting Record '+ (parseInt(this.selectedIndex) + 1) + ' from Timeline');
      $(modalWrapper).find('p').html('You will be deleting "<b>'+ this.selectedModel.caption +'</b>" and the record will be lost forever. Are you sure?');

      $(modalWrapper).find('a.button').on('click', function (e) {
        if ($(e.target).html() == 'Yes') _this.onConfirmDeleteEntry();
        _this.hideModalOverlay(modalWrapper);        
      })
    },

    onConfirmDeleteEntry: function () {
      if (this.selectedModel && this.selectedIndex) {
        defaults.timeline[this.selectedIndex] = '';
        this.refreshTimeline();
      }
    },

    /*---------------------------------------------------------------------
    end of deleting entry handlers
    ---------------------------------------------------------------------*/

    onActionSelected: function (node) {
      this.onResetActionSelection();
      if ($(node).hasClass('selected')) {
        $(node).removeClass('selected');
      } else {
        $(node).addClass('selected');
      }
    },

    onResetActionSelection: function () {
      var actions = $(this.infoView).find('ul.action-list');
      $(actions).find('li a').removeClass('selected');
    },

    /*---------------------------------------------------------------------
    end of action selection handlers
    ---------------------------------------------------------------------*/

    showModalOverlay: function (intoNode) {
      if (intoNode && $(intoNode)) {
        var tmpl = $('#tmpl-modal').html();
        $(intoNode).prepend(tmpl).hide().fadeIn();
      }
    },

    hideModalOverlay: function (modal) {
      if (modal && $(modal)) {
        $(modal).fadeOut(300, function () { $(this).remove(); });
      }
    },

    /*---------------------------------------------------------------------
    end of modal handlers
    ---------------------------------------------------------------------*/

    behavior: function () {
      var _this = this;
      
      $('div.node a.button').on({
        'click': function (e) {
          var node = $(e.target).parent().parent();
          _this.resetSelection();
          $(node).addClass('selected');

          if ($(node).hasClass('add-new') || !defaults.canEdit) {
            // special treatment for this
            _this.currentAction = 'new';
            _this.printInfo('');
            _this.showEditFormWithModel({
              title: '',
              type: 'Normal Connection',
              caption: '',
              info: ''
            });
            return;
          }
          _this.printInfo($(node).attr('data-info'));          
          _this.selectedModel = defaults.timeline[$(node).attr('data-id')];
          _this.selectedIndex = $(node).attr('data-id');          
          _this.onShowText($(e.target).parent());
          _this.showEditFormWithModel();
        }
      });
      // if user doesn't wish to hide text, we disable the interaction here
      if (!defaults.hideTextWhenLoad) return;

      $('div.node a.button').on({
        'mouseover': function (e) {
          _this.onShowText($(e.target).parent());
        },
        'mouseout': function (e) {_this.hideAllText(); }
      });
    },
  }
}();
