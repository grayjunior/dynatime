var dot_templates = {
'Skipped End': '<div class="node end">'+
  '<h2 class="node-title">Sample</h2>'+
  '<a class="button" href="#"><div class="dot-wrapper transparent-dot-end"></div></a>'+
  '<p>Caption</p>'+
  '</div>',
'Unique End': '<div class="node end">'+
  '<h2 class="node-title">Sample</h2>'+
  '<a class="button" href="#"><div class="dot-wrapper lm-dot-end"></div></a>'+
  '<p>Caption</p>'+
  '</div>',
'Normal End': '<div class="node end">'+
  '<h2 class="node-title">Sample</h2>'+
  '<a class="button" href="#"><div class="dot-wrapper white-dot-end"></div></a>'+
  '<p>Caption</p>'+
  '</div>',
'Normal Connection': '<div class="node">'+
  '<h2 class="node-title">Sample</h2>'+
  '<a class="button" href="#"><div class="dot-wrapper white-dot-w-line"></div></a>'+
  '<p>Caption</p>'+
  '</div>',
'Unique Connection': '<div class="node">'+
  '<h2 class="node-title">Sample</h2>'+
  '<a class="button" href="#"><div class="dot-wrapper lm-dot-w-line"></div></a>'+
  '<p>Caption</p>'+
  '</div>',
'Skipped Connection': '<div class="node skipped">'+
  '<h2 class="node-title">Sample</h2>'+
  '<a class="button" href="#"><div class="dot-wrapper transparent-dot-w-line"></div></a>'+
  '<p>Caption</p>'+
  '</div>',
'Add New': '<div class="node end add-new">'+
  '<h2 class="node-title">new?</h2>'+
  '<a class="button" href="#"><div class="dot-wrapper white-dot-end"></div></a>'+
  '<p>Add new event</p>'+
  '</div>',
};

var event_controls = {
  'edit' : '<a id="editAction" class="selected" href="#">Edit event &raquo;</a>',
  'new' : '<a id="addAction" href="#">Add new event &raquo;</a>',
  'delete' : '<a id="deleteAction" class="color-red" href="#">Delete event &raquo;</a>'
};

var type_of_connections = [
  "Normal Connection",
  "Unique Connection",
  "Normal End",
  "Unique End"
];

var contents = [
{
  'title':'$1000',
  'type':'Normal Connection',
  'caption':'Bali expenses',
  'info':'<div class="col-md-8"><h3>Event #1</h3><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p></div>',
  },
  {
  'title':'$1500',
  'type':'Normal Connection',
  'caption':'My new MacAir',
  'info':'<div class="col-md-8"><h3>Event #2</h3><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p></div>',
  },
  {
  'title':'$2500',
  'type':'Unique Connection',
  'caption':'Lorem ipsum dolor.',
  'info':'',    
  },
  {
  'title':'$4000',
  'type':'Normal Connection',
  'caption':'My new TV!',
  'info':'<div class="col-md-8"><h3>Event #4</h3><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p></div>',
  },
  {
  'title':'$4500',
  'type':'Unique Connection',
  'caption':'I owe you money!',
  'info':'',    
  },
  {
  'title':'$5000',
  'type':'Normal Connection',
  'caption':'Australia Expenses',
  'info':'<div class="col-md-8"><h3>Event #6</h3><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p></div>',
  },
  {
  'title':'$5000',
  'type':'Skipped End',
  'caption':'A wedding ring',
  'info':'<div class="col-md-8"><h3>Event #7</h3><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p></div>',
  },
];